import argparse
import requests
import threading
import csv

parser = argparse.ArgumentParser()
parser.add_argument('urls_file', metavar='u', type=str)
parser.add_argument('subpaths_file', metavar='s', type=str)
parser.add_argument('output_file', metavar='o', type=str, default=None)

args = parser.parse_args()

with open(args.urls_file) as urls_file, open(args.subpaths_file) as subpaths_file:
    urls = {'http://' + url.strip() for url in urls_file}
    subpaths = {subpath.strip() for subpath in subpaths_file}

exists = {}
current = 0
threads = []

def scrap(url, subpaths):
    global exists
    global current
    current += 1
    for subpath in subpaths:
        try:
            request = requests.get("{}{}".format(url, subpath))
        except requests.exceptions.ConnectionError:
            exists['{}{}'.format(url, subpath)] = False
        else:
            if request.url == "{}{}".format(url, subpath):
                exists['{}{}'.format(url, subpath)] = request.status_code == 200
            else:
                exists['{}{}'.format(url, subpath)] = False
    progress = 100 * (1 - (current/len(threads)))
    print("Progress: {:.2f}%".format(progress))
    current -= 1

for url in urls:
    threads.append(threading.Thread(target=scrap, args=(url, subpaths)))
    threads[-1].start()

while current:
    pass

if args.output_file:
    with open(args.output_file, 'w') as output_file:
        dict_writer = csv.writer(output_file, )
        dict_writer.writerows(exists.items())
else:
    print(exists)
